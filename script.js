// Obtener todos los elementos de la galería
const galleryItems = document.querySelectorAll('.gallery-item');
const modal = document.getElementById('modal');
const modalImg = document.getElementById('modal-img');
const closeBtn = document.querySelector('.close');

function openModal(imgSrc) {
  modalImg.src = imgSrc;
  modal.style.display = 'block';
}

function closeModal() {
  modal.style.display = 'none';
}

galleryItems.forEach(item => {
  const img = item.querySelector('.gallery-image');
  img.addEventListener('click', () => openModal(img.src));
});

closeBtn.addEventListener('click', closeModal);

window.addEventListener('click', (event) => {
  if (event.target === modal) {
    closeModal();
  }
});